import { FAVOR } from "../../constants";
import { setStateToLS } from "../../utils";
import { SET_ITEMS, SET_FAVOURITES, TOGGLE_FAVOURITE_ITEM } from "./action";

const initialState = {
  allItems: [],
  favItems: [],
};

export const itemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ITEMS: {
      return { ...state, allItems: action.payload };
    }
    case SET_FAVOURITES: {
      return {
        ...state,
        favItems: action.payload,
      };
    }
    case TOGGLE_FAVOURITE_ITEM: {
      const newItems = [...state.favItems];
      const item = action.payload;

      const index = newItems.findIndex(el => el.id === item.id);
      if (index !== -1) {
        newItems.splice(index, 1);
      } else {
        newItems.push(item);
      }

      setStateToLS(FAVOR, newItems);

      return {
        ...state,
        favItems: newItems,
      };
    }

    default: {
      return state;
    }
  }
};
