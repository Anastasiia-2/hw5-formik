export const SET_ITEMS = "SET_ITEMS";

export const SET_FAVOURITES = "SET_FAVOURITES";

export const TOGGLE_FAVOURITE_ITEM = "TOGGLE_FAVOURITE_ITEM";
