import { setStateToLS } from "../../utils";
import { SET_CART_ITEMS, ADD_TO_CART, REMOVE_FROM_CART } from "./action";
import { CART } from "../../constants";

const initialState = {
  cartItems: [],
  fullAmount: 0,
};

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CART_ITEMS: {
      const items = action.payload;
      const fullAmount = items.reduce((acc, el) => {
        return (acc += el.count);
      }, 0);
      return { ...state, cartItems: items, fullAmount };
    }
    case ADD_TO_CART: {
      const item = action.payload;
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(el => el.id === item.id);
      if (index !== -1) {
        newCartItems[index].count++;
      } else {
        item.count = 1;
        newCartItems.push(item);
      }
      let fullAmount = state.fullAmount;
      fullAmount++;
      setStateToLS(CART, newCartItems);
      return { ...state, fullAmount, cartItems: newCartItems };
    }

    case REMOVE_FROM_CART: {
      const id = action.payload;
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(el => el.id === id);
      const item = newCartItems[index];
      item.count -= 1;

      if (!item.count) {
        newCartItems.splice(index, 1);
      }

      let fullAmount = state.fullAmount;
      fullAmount -= 1;

      setStateToLS(CART, newCartItems);
      return { ...state, fullAmount, cartItems: newCartItems };
    }

    default: {
      return state;
    }
  }
};
