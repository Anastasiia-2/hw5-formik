import { getStateFromLS, removeItemFromLS } from "../../utils";
import { SET_CART_ITEMS, ADD_TO_CART, REMOVE_FROM_CART } from "./action";
import { CART } from "../../constants";

export const setCartItems = items => {
  return { type: SET_CART_ITEMS, payload: items };
};

export const addToCart = item => {
  return { type: ADD_TO_CART, payload: item };
};

export const removeFromCart = item => {
  return { type: REMOVE_FROM_CART, payload: item.id };
};

export const fetchCartItems = () => {
  return dispatch => {
    const items = getStateFromLS(CART) ? getStateFromLS(CART) : [];
    dispatch(setCartItems(items));
  };
};

export const resetCartItems = () => {
  return dispatch => {
    const items = [];
    dispatch(setCartItems(items));
    removeItemFromLS(CART);
  };
};
