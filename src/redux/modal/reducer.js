import {
  TOGGLE_MODAL,
  SET_MODAL_HEADER,
  SET_MODAL_TEXT,
  SET_MODAL_CALLBACK,
} from "./action";

const initialState = {
  isModalOpen: false,
  header: "",
  text: "",
  callback: () => {},
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODAL: {
      return { ...state, isModalOpen: !state.isModalOpen };
    }

    case SET_MODAL_HEADER: {
      return { ...state, header: action.payload };
    }

    case SET_MODAL_TEXT: {
      return { ...state, text: action.payload };
    }
    case SET_MODAL_CALLBACK: {
      return { ...state, callback: action.payload };
    }

    default: {
      return state;
    }
  }
};
