import React, { memo } from "react";
import { MdChair } from "react-icons/md";
import styles from "./Logo.module.scss";

function Logo() {
  return (
    <a href="/" className={styles.logo}>
      <span>{<MdChair />}</span> Furniking
    </a>
  );
}

export default memo(Logo);
