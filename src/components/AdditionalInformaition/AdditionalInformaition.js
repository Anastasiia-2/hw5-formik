import React from "react";
import { useSelector } from "react-redux";
import { AiOutlineHeart } from "react-icons/ai";
import { BsCart } from "react-icons/bs";
import styles from "./Additional.module.scss";

export default function AdditionalInformaition() {
  const favCount = useSelector(state => state.items.favItems?.length) || 0;
  const cartCount = useSelector(state => state.cart.fullAmount);

  return (
    <div className={styles.additionalWrapper}>
      <p className={styles.actions}>
        <BsCart size="24" />
        <span className={styles.actionsCount}>{cartCount}</span>
      </p>

      <p className={styles.actions}>
        <AiOutlineHeart size="24" />
        <span className={styles.actionsCount}>{favCount}</span>
      </p>
    </div>
  );
}
