import React from "react";
import styles from "./Header.module.scss";

import Logo from "../Logo/Logo";
import AdditionalInformaition from "../AdditionalInformaition/AdditionalInformaition";
import PageNavigation from "../PageNavigation/PageNavigation";

export default function Header() {
  return (
    <section className={styles.pageHeader}>
      <div className={styles.greetings}>
        <p>Welcome to our online shop </p>
      </div>
      <header className={styles.headerContainer}>
        <Logo />
        <PageNavigation />

        <AdditionalInformaition />
      </header>
    </section>
  );
}
