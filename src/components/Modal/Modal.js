import { MdClose } from "react-icons/md";
import styles from "./Modal.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { toggleModal } from "../../redux/modal/actionCreators";

function Modal() {
  const header = useSelector(state => state.modal.header);
  const text = useSelector(state => state.modal.text);
  const callback = useSelector(state => state.modal.callback);

  const dispatch = useDispatch();

  const closeModal = () => {
    dispatch(toggleModal());
  };

  const handleConfirmBtn = () => {
    callback();
    closeModal();
  };

  return (
    <div
      data-wrapper
      className={styles.backdrop}
      onClick={e => e.target.dataset.wrapper && closeModal()}
    >
      <div className={styles.modal}>
        <div className={styles.containerHeader}>
          <h2 className={styles.title}>{header}</h2>

          <button onClick={closeModal} className={styles.closeButton}>
            {<MdClose />}
          </button>
        </div>
        <p className={styles.text}>{text}</p>

        <div>
          <button onClick={handleConfirmBtn} className={styles.button}>
            Yes
          </button>
          <button onClick={closeModal} className={styles.button}>
            No
          </button>
        </div>
      </div>
    </div>
  );
}

export default Modal;
