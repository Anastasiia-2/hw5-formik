import React from "react";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";

export default function ItemsContainer({ items }) {
  return (
    <ul className={styles.list}>
      {items?.map(item => {
        return (
          <li key={item?.id} className={styles.item}>
            <Item item={item} />
          </li>
        );
      })}
    </ul>
  );
}
