import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import AppRoutes from "./AppRoutes";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import { fetchFavourites, fetchItems } from "./redux/items/actionCreators";
import { fetchCartItems } from "./redux/cart/actionCreators";
import "./App.css";

function App() {
  const dispatch = useDispatch();
  const isModalOpen = useSelector(state => state.modal.isModalOpen);

  useEffect(() => {
    dispatch(fetchItems());
    dispatch(fetchFavourites());
    dispatch(fetchCartItems());
  }, [dispatch]);

  return (
    <div className="wrapper">
      <Header />
      <div className="App">
        <AppRoutes />
        {isModalOpen && <Modal />}
      </div>
    </div>
  );
}

export default App;
