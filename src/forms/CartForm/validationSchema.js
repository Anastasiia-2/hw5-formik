import { object, string, number } from "yup";

const validationSchema = object({
  name: string().trim().min(3).required("name is required"),
  surname: string().trim().min(3).required("surname is required"),
  age: number()
    .min(12, "age must be over 12 years old")
    .required("age is required"),
  deliveryAddress: string().trim().required("delivery address is required"),
  mobileNumber: number().required("mobile number is required"),
});
export default validationSchema;
